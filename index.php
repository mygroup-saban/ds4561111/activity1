<?php require_once "./code.php"; ?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DocumentS01: Activity</title>
</head>
<body>
    <h1>Full Address</h1>
    
    <div style="width: 300px; height: 100px; border: 1px solid black; padding: 10px;">
        <!-- use getFullAddress Function -->
    <p><?= getFullAddress('Philippines', 'Metro Manila', 'Quezon City', '3F, Caswynn Bldg., Timog Avenue'); ?></p>
    <p><?= getFullAddress('Philippines', 'Metro Manila', 'Makati City', '3F, CEnzo Bldg.,'); ?></p>
    </div>

    <h1>Letter - Based Grading</h1>
    <div style="width: 300px; height: 100px; border: 1px solid black; padding: 10px;">
        <!-- use getLetterGrade Function -->
        <p><?= getLetterGrade(87); ?></p>
        <p><?= getLetterGrade(94); ?></p>
        <p><?= getLetterGrade(74); ?></p>
    </div>

    

    
</body>
</html>